package com.example.dummyecom.dataprovider

import com.example.dummyecom.model.Cateogary
import com.example.dummyecom.model.Product

object DataProvider {

    val categories = listOf(

        Cateogary("Hats","hatimage"),
        Cateogary("Hoodies","hoodieimage"),
        Cateogary("Shirts","shirtimage"),
        Cateogary("Digital Accessories","digitalgoodsimage")
    )


    val productsHats = listOf(

        Product("Black Beannie","INR 199","hat1"),
        Product("Cap Black","INR 449","hat2"),
        Product("Cap White","INR 99","hat3"),
        Product("Cap Mix Design","INR 599","hat4")

    )


    val productsShirts = listOf(

        Product("T-Shirt Black","INR 2199","shirt1"),
        Product("T-Shirt Grey","INR 2449","shirt2"),
        Product("T-Shirt Red","INR 1199","shirt3"),
        Product("T-Shirt Swift Delegate","INR 2599","shirt4"),
        Product("T-Shirt Kick Flag Studio","INR 1349","shirt5")
    )


    val productsHoodies = listOf(

        Product("Hoodie Black","INR 1199","hoodie1"),
        Product("Hoodie Red","INR 1449","hoodie2"),
        Product("Hoodie DevSlopes","INR 1299","hoodie3"),
        Product("Hoodie Warmer","INR 1599","hoodie4"),
        Product("Hoodie Black","INR 1199","hoodie1"),
        Product("Hoodie Red","INR 1449","hoodie2"),
        Product("Hoodie DevSlopes","INR 1299","hoodie3"),
        Product("Hoodie Warmer","INR 1599","hoodie4")

    )


    val digitalGoodies = listOf<Product>()



    fun getRequiredProductListingDataFor(cateogary:String): List<Product> {

        return when(cateogary) {
            "Hats" -> productsHats
            "Hoodies" -> productsHoodies
            "Shirts" -> productsShirts
            "Digital Accessories" -> digitalGoodies
            else -> digitalGoodies
        }


    }
}