package com.example.dummyecom.controller

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyecom.R
import com.example.dummyecom.adapter.CustomAdapter
import com.example.dummyecom.adapter.CustomNewListViewAdapter
import com.example.dummyecom.dataprovider.DataProvider
import com.example.dummyecom.model.Cateogary
import kotlinx.android.synthetic.main.activity_index_page.*


const val EXTRA_CATEOGARY = "cateogary"

class IndexPageActivity : AppCompatActivity() {

    lateinit var adapter: CustomNewListViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_index_page)

        val cateogaries = DataProvider.categories

        customListView.adapter = CustomNewListViewAdapter(this, cateogaries){ cateogary ->
            val activityIntent = Intent(this, ProductListingActivity::class.java)
            activityIntent.putExtra(EXTRA_CATEOGARY,cateogary)
            startActivity(activityIntent)
        }
        val layoutManger = LinearLayoutManager(this)
        customListView.layoutManager = layoutManger
    }
}
