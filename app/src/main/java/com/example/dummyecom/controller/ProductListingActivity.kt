package com.example.dummyecom.controller


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.dummyecom.R
import com.example.dummyecom.adapter.ProductListingAdapter
import com.example.dummyecom.dataprovider.DataProvider
import com.example.dummyecom.model.Cateogary
import kotlinx.android.synthetic.main.activity_index_page.*
import kotlinx.android.synthetic.main.activity_product_listing.*

class ProductListingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_listing)


        val cateogary:Cateogary =  intent.getParcelableExtra(EXTRA_CATEOGARY)

        val getString = cateogary.title!!

        val requiredProductListing = DataProvider.getRequiredProductListingDataFor(getString)

        productListingView.adapter = ProductListingAdapter(this,requiredProductListing)

        val layoutManger = GridLayoutManager(this,2)

        productListingView.layoutManager = layoutManger
    }
}
