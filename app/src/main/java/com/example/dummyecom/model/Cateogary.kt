package com.example.dummyecom.model

import android.os.Parcel
import android.os.Parcelable

class Cateogary(val title: String?, val image: String?): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Cateogary> {
        override fun createFromParcel(parcel: Parcel): Cateogary {
            return Cateogary(parcel)
        }

        override fun newArray(size: Int): Array<Cateogary?> {
            return arrayOfNulls(size)
        }
    }
}