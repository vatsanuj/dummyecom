package com.example.dummyecom.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyecom.R
import com.example.dummyecom.model.Product

class ProductListingAdapter(val context:Context, val productListing:List<Product>):
    RecyclerView.Adapter<ProductListingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(context).inflate(R.layout.product_listing_layout,null)
        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return  productListing.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindViewWithDate(context,productListing[position])
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val productName: TextView = itemView.findViewById(R.id.productName)
        val productImage: ImageView = itemView.findViewById(R.id.produtImage)
        val productPrice: TextView = itemView.findViewById(R.id.productPrice)

        fun bindViewWithDate(context: Context, productDetail: Product){
            productName.text = productDetail.image
            productPrice.text = productDetail.price

            val resourceID = context.resources.getIdentifier(productDetail.image,"drawable",context.packageName)
            productImage.setImageResource(resourceID)
        }

    }

}