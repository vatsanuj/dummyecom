package com.example.dummyecom.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.dummyecom.R
import com.example.dummyecom.model.Cateogary


class CustomNewListViewAdapter(val context: Context , val categories: List<Cateogary>, val cateogaryClicked: (Cateogary) -> Unit ):
    Adapter<CustomNewListViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val holderView = LayoutInflater.from(context).inflate(R.layout.custom_list_layout,null)
        val viewHolder = ViewHolder(holderView,cateogaryClicked)
        return  viewHolder
    }

    override fun getItemCount(): Int {
        return categories.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(context,categories[position])
    }

    inner class ViewHolder(itemView: View, cateogaryClicked: (Cateogary) -> Unit):RecyclerView.ViewHolder(itemView) {

         var catName: TextView = itemView.findViewById(R.id.cateogaryName)
         var catImage: ImageView = itemView.findViewById(R.id.cateogaryImage)

         fun bindView(context: Context, category:Cateogary){
             val imageResource = context.resources.getIdentifier(category.image, "drawable",context.packageName)
             catName.text = category.title
             catImage.setImageResource(imageResource)

            itemView.setOnClickListener { cateogaryClicked(category) }

         }



    }
}