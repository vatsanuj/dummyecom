package com.example.dummyecom.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.dummyecom.R
import com.example.dummyecom.model.Cateogary


class CustomAdapter(val context:Context , val categories: List<Cateogary> ): BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var convertView : View

        convertView = LayoutInflater.from(context).inflate(R.layout.custom_list_layout,null)
        val categoryName:TextView =  convertView.findViewById(R.id.cateogaryName)
        val categoryImage:ImageView = convertView.findViewById(R.id.cateogaryImage)

        val indexCateogry = categories[position]

        val imageResource = context.resources.getIdentifier(indexCateogry.image, "drawable",context.packageName)

        categoryName.text = indexCateogry.title

        categoryImage.setImageResource(imageResource)

        return convertView
    }

    override fun getItem(position: Int): Any {
        return categories[position]
    }

    override fun getItemId(position: Int): Long {
        return  0
    }

    override fun getCount(): Int {
      return categories.count()
    }
}